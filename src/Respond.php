<?php

namespace Larasar\Helpers;

use Illuminate\Http\Response;
use Laraquick\Controllers\Traits\Response\Api;
use Laraquick\Controllers\Traits\Response\Common;

class Respond
{
  use Api, Common;

  public static function __callStatic($name, $args = [])
  {
    return call_user_func_array([new static, $name], $args);
  }

  public function __call($name, $args = [])
  {
    if (method_exists($this, $name)) {
      return call_user_func_array([$this, $name], $args);
    }
  }

  protected function error($message, $errors = null, $code = Response::HTTP_BAD_REQUEST)
  {
    $resp = [
      "status" => "error",
      "status_code" => $code,
      "message" => $this->translate($message),
    ];

    if ($errors !== null) {
      $resp["errors"] = $errors;
    }

    return response()->json($resp, $code);
  }

  protected function translate($text): string
  {
    return _t($text);
  }
}
