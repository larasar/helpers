<?php

namespace Larasar\Helpers\Notifications;

class DatabaseData
{

  private $code;
  private $text;
  private $textOptions;
  private $actionUrl;
  private $actionLabel;
  private $subject;
  private $subjectOptions;
  private $isError = false;

  /**
   * The notification code
   *
   * @param integer $code
   * @return self
   */
  public function setCode($code): self
  {
    $this->code = $code;

    return $this;
  }

  /**
   * The notification text message.
   *
   * This may contain translation placeholders.
   *
   * @param string $text
   * @param array $options Array of translation placeholder values
   * @return self
   */
  public function setText($text, array $options = []): self
  {
    $this->text = $text;
    $this->textOptions = $options;

    return $this;
  }

  /**
   * Sets the action for the notification
   *
   * @param string $url
   * @param string $label
   * @return self
   */
  public function setAction($url, $label = null): self
  {
    $this->actionUrl = $url;
    $this->actionLabel = $label;

    return $this;
  }

  /**
   * Sets the notification subject
   *
   * This may contain translation placeholders.
   *
   * @param string $subject
   * @param array $options Array of translation placeholder values
   * @return self
   */
  public function setSubject($subject, array $options = []): self
  {
    $this->subject = $subject;
    $this->subjectOptions = $options;

    return $this;
  }

  public function setIsError(bool $isError = true): self
  {
    $this->isError = $isError;

    return $this;
  }

  public function getCode()
  {
    return $this->code;
  }

  public function getText()
  {
    return $this->text;
  }

  public function getTextOptions()
  {
    return $this->textOptions;
  }

  public function getActionUrl()
  {
    return $this->actionUrl;
  }

  public function getActionLabel()
  {
    return $this->actionLabel;
  }

  public function getIsError(): bool
  {
    return $this->isError;
  }

  public function toArray()
  {
    return [
      'code' => $this->code,
      'subject' => $this->subject,
      'subject_options' => $this->subjectOptions,
      'text' => $this->text,
      'text_options' => $this->textOptions,
      'action_url' => $this->actionUrl,
      'action_label' => $this->actionLabel,
      'email_sent' => false,
      'is_error' => $this->isError,
    ];
  }
}
