<?php

namespace Larasar\Helpers;

use Exception;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Laraquick\Events\WebSocket;

class Broadcast
{
  private string $channelName;
  private bool $private = true;
  private bool $public = false;
  private bool $presence = false;
  private bool $enabled = true;

  public function public(string $channelName): self
  {
    $this->channelName = $channelName;

    $this->public = true;
    $this->private = false;
    $this->presence = false;

    return $this;
  }

  public function private(string $channelName): self
  {
    $this->channelName = $channelName;

    $this->private = true;
    $this->public = false;
    $this->presence = false;

    return $this;
  }

  public function presence(string $channelName): self
  {
    $this->channelName = $channelName;

    $this->presence = true;
    $this->public = false;
    $this->private = false;

    return $this;
  }

  public function enable(): self
  {
    $this->enabled = true;

    return $this;
  }

  public function disable(): self
  {
    $this->enabled = false;

    return $this;
  }

  public function event(string $name, array $data)
  {
    return $this->enabled ?
      broadcast(new WebSocket($this->createChannel(), $name, $data)) :
      optional();
  }

  private function createChannel(): Channel
  {
    if (!$this->channelName) {
      throw new Exception('Invalid channel name');
    }

    if ($this->public) {
      return new Channel($this->channelName);
    } else if ($this->private) {
      return new PrivateChannel($this->channelName);
    } else if ($this->presence) {
      return new PresenceChannel($this->channelName);
    }

    throw new Exception('Invalid channel type');
  }
}
