<?php

namespace Larasar\Helpers\Validation;

use Illuminate\Validation\Rules\Unique;

class UniqueRule extends Unique
{
  public function __construct(string $tableName, string $columnName = 'NULL', array $otherFields = [])
  {
    parent::__construct($tableName, $columnName);

    $this->withOtherFields(array_merge(['deleted_at' => null], $otherFields));
  }

  public function withId($id): self
  {
    return $this->ignore($id);
  }

  public function withOtherFields(array $fields): self
  {
    foreach ($fields as $column => $value) {
      $this->with($column, $value);
    }

    return $this;
  }

  public function with(string $key, $value): self
  {
    if ($key === 'id') {
      return $this->withId($value);
    }

    return $this->where($key, $value);
  }
}
