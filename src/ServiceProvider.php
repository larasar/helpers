<?php

namespace Larasar\Helpers;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
