<?php

namespace Larasar\Helpers;

class Frontend
{
  public static function view($action, $queryParams = [])
  {
    $url = config('larasar.frontend.url') . '/view?action=' . $action;

    if (count($queryParams)) {
      $url .= '&' . http_build_query($queryParams);
    }

    return $url;
  }
}
